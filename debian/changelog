kup (0.3.6-4) unstable; urgency=medium

  * Upload to unstable

 -- Ben Hutchings <benh@debian.org>  Wed, 15 May 2024 13:04:06 +0200

kup (0.3.6-3) experimental; urgency=medium

  [ Ben Hutchings ]
  * debian/control: Change to team-maintained, with me as Uploader
  * debian/control: Use my debian.org email in Uploaders field
  * d/.gitignore: Ignore more debhelper temporary files
  * d/salsa-ci.yml: Add CI configuration for salsa.debian.org
  * kup-server: Remove obsolete dependency on lsb-base

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Ben Hutchings <benh@debian.org>  Sat, 25 Nov 2023 14:11:31 +0000

kup (0.3.6-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:13:32 +0200

kup (0.3.6-2) unstable; urgency=medium

  * debian/control: Point Vcs URLs to Salsa
  * Add upstream public key and debian/watch file
  * debian/copyright: Use https: scheme in Format
  * debian/control: Change Priority to optional
  * debian/control: Set Rules-Requires-Root: no
  * debian/control: Update Standards-Version to 4.4.1
  * lintian: Override false positive debian-watch-could-verify-download
    (see #950277)
  * Add systemd service file for kup-server and enable dh systemd integration
  * Upload to unstable

 -- Ben Hutchings <ben@decadent.org.uk>  Fri, 31 Jan 2020 00:07:08 +0100

kup (0.3.6-1) experimental; urgency=medium

  * New upstream version, including changes needed to work with kernel.org in
    future (Closes: #859143):
    - Add support for subcmd config option
    - Make sure we use sanitized KUP_SUBCMD
  * kup-server: Update recommended packages to match the new upstream default
    configuration
  * Use debhelper compatibility level 9
  * debian/control: Update Standards-Version to 3.9.8; no changes needed
  * kup-server: Remove obsolete versioned dependency on base-files (for /run)
  * debian/copyright: Update to machine-readable format version 1.0
  * kup-server: Add dependency on lsb-base (for /lib/lsb/init-functions)

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 09 Apr 2017 03:42:08 +0100

kup (0.3.4-2) unstable; urgency=medium

  * debian/control: Change Vcs-Git, Vcs-Browser and Homepage to canonical
    HTTP-S URLs
  * debian/copyright: Change Source to HTTP-S URL
  * Upload to unstable

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 30 Aug 2015 13:33:51 +0100

kup (0.3.4-1) experimental; urgency=low

  * New upstream release

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 10 Mar 2013 02:52:51 +0000

kup (0.3.2-1) unstable; urgency=low

  * Initial release (Closes: #648896)

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 04 Dec 2011 02:19:45 +0000
